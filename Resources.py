# The file containing the main paths to resources the evaluation needs or produces
import os


##########
# Images #
##########
# The path to the (root) images directory
root_images_directory = "/../Images"

# The directory name for the test images directory (1 - 1000)
test_images_directory_name = "test"
# The directory name for the train images directory (1001 - 4000)
train_images_directory_name = "train"


###########
# Results #
###########
# The path to the (root) results directory
root_results_directory = "../../Results"


# Procedure to get the test images directory for a given object class
def get_test_image_directory(object_class):
    return os.path.join(root_images_directory, object_class, test_images_directory_name)


# Procedure to get the train images directory for a given object class
def get_train_image_directory(object_class):
    return os.path.join(root_images_directory, object_class, train_images_directory_name)
