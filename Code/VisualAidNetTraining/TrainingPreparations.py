from GatherPredictions.GatherPredictions import image_to_numpy_array
from VisualAidNetTraining.VisualAidNetModel import object_class_to_index
from ObjectDictionaries import *
from Resources import get_train_image_directory
import numpy
import os


# Procedure to preprocess all (train & validation) images for all available categories
def all_images_to_numpy_arrays(preprocess_procedure, target_size, validation_split=0.25, data_size=1):
    # The number of object classes, and the number of images per class
    number_of_object_classes = len(all_object_classes)
    images_per_class = int(3000 * data_size)

    # Split the images into a training and validation test
    train_split = 1 - validation_split
    split_index_per_class = int(images_per_class * train_split)
    train_images_per_class = split_index_per_class
    validation_images_per_class = images_per_class - train_images_per_class

    # The array of train preprocessed images (for all classes)
    train_preprocessed_images = numpy.full((train_images_per_class * number_of_object_classes,
                                            target_size[0], target_size[1], 3), None)
    # The array of validation preprocessed images (for all classes)
    validation_preprocessed_images = numpy.full((validation_images_per_class * number_of_object_classes,
                                                 target_size[0], target_size[1], 3), None)

    # For each object class
    for idx, object_class in enumerate(all_object_classes):
        # Get the train directory of the object class
        images_directory_path = get_train_image_directory(object_class)
        # Extract the file names of the images (all images should be .jpg files (as prepared)
        images_file_names = [f for f in os.listdir(images_directory_path) if f.endswith('.jpg')]
        number_of_images = len(images_file_names)
        print(object_class, "images", "(" + str(idx + 1) + "/" + str(len(all_object_classes)) + ")",
              "\n--------------------------")
        print(number_of_images, "images found in directory.")
        # Adjust to the data size, if needed
        if data_size == 1:
            print("Using all images for data_size =", data_size)
        elif 0 < data_size < 1:
            max_index = int(number_of_images * data_size)
            images_file_names = images_file_names[0:max_index]
            print("Using only", max_index, "of", number_of_images, "images for data_size =", data_size)
        else:
            print("Invalid data_size:", data_size, ". Expected:  0 < data_size < 1\nUsing all images.")

        print("Preprocessing images...")

        # Split images into training and validation
        train_images = images_file_names[0:split_index_per_class]
        validation_images = images_file_names[split_index_per_class:]

        # Iterate over the training images
        for sub_idx, image_name in enumerate(train_images):
            # Get the path to the image
            image_path = os.path.join(images_directory_path, image_name)
            # Preprocess the image
            image_array = image_to_numpy_array(image_path, preprocess_procedure, target_size)
            # Add it to the list of preprocessed images
            train_preprocessed_images[idx * train_images_per_class + sub_idx] = image_array

        # Iterate over the validation images
        for sub_idx, image_name in enumerate(validation_images):
            # Get the path to the image
            image_path = os.path.join(images_directory_path, image_name)
            # Preprocess the image
            image_array = image_to_numpy_array(image_path, preprocess_procedure, target_size)
            # Add it to the list of preprocessed images
            validation_preprocessed_images[idx * validation_images_per_class + sub_idx] = image_array

        print("Train images: ", train_images_per_class, ", Validation images: ", validation_images_per_class, sep="")
        print(object_class, "images preprocessed!\n")

    print(len(train_preprocessed_images), "preprocessed training images.")
    print(len(validation_preprocessed_images), "preprocessed validation images.")
    print("All images preprocessed!\n")

    # Return the preprocessed images
    return train_preprocessed_images, validation_preprocessed_images


# Procedure to create the target data for a given set of training and validation images
def create_all_target_data(train_preprocessed_images, validation_preprocessed_images,
                           validation_split=0.25, data_size=1):
    images_per_class = int(3000 * data_size)
    target_size = len(all_object_classes)

    # Split the images into a training and validation test
    train_split = 1 - validation_split
    train_images_per_class = int(images_per_class * train_split)
    validation_images_per_class = images_per_class - train_images_per_class

    # Target outputs for train images
    train_target_outputs = numpy.full((len(train_preprocessed_images), target_size), numpy.zeros(target_size))
    # Target output for validation images
    validation_target_outputs = numpy.full((len(validation_preprocessed_images), target_size), numpy.zeros(target_size))

    print("Creating target data...")
    # For each object class
    for idx, object_class in enumerate(all_object_classes):
        print(object_class, "images target output", "(" + str(idx + 1) + "/" + str(len(all_object_classes)) + ")",
              "\n--------------------------\nGenerating target output...")
        # The target output
        target_output = numpy.zeros(target_size)
        target_index = object_class_to_index(object_class)
        target_output[target_index] = 1
        # Update the target output in the training and validation target outputs
        train_target_outputs[idx*train_images_per_class:(idx+1)*train_images_per_class] = target_output
        validation_target_outputs[idx*validation_images_per_class:(idx+1)*validation_images_per_class] = target_output

        print(object_class, "target output preprocessed!\n")

    print("Target data created!")
    return train_target_outputs, validation_target_outputs
