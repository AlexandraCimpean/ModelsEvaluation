from keras import metrics
from keras.optimizers import RMSprop

from VisualAidNetTraining.TrainingPreparations import *
from VisualAidNetTraining.VisualAidNetModel import VisualAidNet, preprocess_input
import keras.callbacks as callbacks
import warnings


# Procedure to train a VisualAidNet
def train_VisualAidNet(width_multiplier, X_train, y_train, X_val, y_val,
                       number_of_epochs, current_epoch=0, learning_rate=0.001, batch_size=32,
                       load_weights=None):
    warnings.filterwarnings('ignore')

    # Set the seed
    # PYTHONHASHSEED=0 added to interpreter
    seed = 1000
    import random, tensorflow, numpy
    random.seed(seed)
    tensorflow.set_random_seed(seed)
    numpy.random.seed(seed)

    # Alpha == width_multiplier in [0.25, 0.50, 0.75, 1]
    alpha = width_multiplier
    # Depth multiplier
    depth_multiplier = 1

    # Input shape => include_top must be set to False to change the input shape
    input_shape = (128, 128, 3)

    # The weights to load in, or None
    weights = load_weights

    print("Loading model...", "\n==========================\n")
    # Load in the model
    model = VisualAidNet(input_shape=input_shape, include_top=False,
                         alpha=alpha, depth_multiplier=depth_multiplier,
                         weights=weights)

    print("Model loaded!\n")

    # The set of validation data
    # The reason we do this ourselves is to assure that the model gets the same amount of validation for each class
    validation_data = (X_val, y_val)

    # The initial epoch at training start (used to resume a training session)
    initial_epoch = current_epoch  # 0
    # The number of training epochs/iterations over the data
    epochs = number_of_epochs  # 100

    # The optimizer
    optimizer = RMSprop(lr=learning_rate)
    # The loss function
    loss_function = 'mean_squared_error'  # 'categorical_crossentropy'  #
    # The metrics
    training_metrics = [metrics.categorical_accuracy]

    # Save the models weights, after checkpoint_period epochs
    checkpoint_period = 1
    CheckpointCallback = callbacks.ModelCheckpoint("../Models/VisualAidNet_Weights/VisualAidNet_alpha=" + str(alpha) +
                                                   "_epoch={epoch:03d}_val_loss={val_loss:.2f}.hdf5",
                                                   period=checkpoint_period)
    # Log the training results
    LoggerCallback = callbacks.CSVLogger("../Models/VisualAidNet_Weights/VisualAidNet_alpha=" + str(alpha) +
                                         "-data_size=" + str(data_size) + '-training.csv')
    # The list of callbacks to use during training
    training_callbacks = [CheckpointCallback, LoggerCallback]

    print("Compiling model...", "\n==========================\n")
    # Compile/prepare the model for training
    model.compile(optimizer=optimizer, loss=loss_function, metrics=training_metrics,
                  loss_weights=None, sample_weight_mode=None, weighted_metrics=None, target_tensors=None)
    print("Model compiled!\n")

    print("Training model...", "\n==========================\n")
    # Train the model to fit the data
    history = model.fit(x=X_train, y=y_train, batch_size=batch_size, epochs=epochs, initial_epoch=initial_epoch,
                        verbose=2, callbacks=training_callbacks,
                        validation_split=validation_split, shuffle=True, validation_data=validation_data,
                        class_weight=None, sample_weight=None, steps_per_epoch=None, validation_steps=None)
    print("Model finished training!")

    warnings.filterwarnings('always')


if __name__ == '__main__':
    # Create training data (this can take a while, as there are a lot of images)
    print("Preprocessing...", "\n==========================\n")

    # The validation split = percentage of data to use as validation
    validation_split = 0.25
    # The data_size to load in (0 < data_size <= 1)
    data_size = 0.5

    # The target size of the images
    target_size = (128, 128)
    # All object classes
    X_train, X_val = all_images_to_numpy_arrays(preprocess_input, target_size=target_size,
                                                validation_split=validation_split, data_size=data_size)
    y_train, y_val = create_all_target_data(X_train, X_val, validation_split=validation_split, data_size=data_size)
    print("Finished preprocessing!\n")

    epochs = 100

    print("\n\n\nVisualAidNet with alpha = 0.75\n")
    train_VisualAidNet(0.75, X_train, y_train, X_val, y_val, epochs, current_epoch=0, learning_rate=0.001,
                       batch_size=32, load_weights=None)

    print("\n\n\nVisualAidNet with alpha = 0.50\n")
    train_VisualAidNet(0.50, X_train, y_train, X_val, y_val, epochs, current_epoch=0, learning_rate=0.001,
                       batch_size=32, load_weights=None)
    print("\n\n\nVisualAidNet with alpha = 0.25\n")
    train_VisualAidNet(0.25, X_train, y_train, X_val, y_val, epochs, current_epoch=0, learning_rate=0.001,
                       batch_size=32, load_weights=None)
    print("VisualAidNet with alpha = 1\n")
    train_VisualAidNet(1, X_train, y_train, X_val, y_val, epochs, current_epoch=0, learning_rate=0.001,
                       batch_size=32, load_weights=None)
