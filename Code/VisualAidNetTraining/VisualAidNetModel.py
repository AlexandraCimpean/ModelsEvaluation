# The architecture of VisualAidNet, based on MobileNet
from keras import backend as K, Model
from keras import Input
from keras.applications import imagenet_utils
from keras.applications.imagenet_utils import _obtain_input_shape
from keras.applications.mobilenet import _conv_block, _depthwise_conv_block
from keras.engine import get_source_inputs
from keras.layers import GlobalAveragePooling2D, Reshape, Dropout, Conv2D, Activation
from ObjectDictionaries import all_object_classes
import numpy
import os
import warnings


# Procedure to map an object class onto an index
def object_class_to_index(object_class):
    return numpy.where(all_object_classes == object_class)[0][0]


# Procedure to map an index to an object class
def index_to_object_class(index):
    return all_object_classes[index]


# Decode predictions (based on MobileNet's implementation)
def decode_predictions(predictions, top=5):
    if predictions.shape[1] != len(all_object_classes):
        raise ValueError('`decode_predictions` expects '
                         'a batch of predictions '
                         '(i.e. a 2D array of shape (samples, ' + str(len(all_object_classes)) +
                         'Found array with shape: ' + str(predictions.shape))
    results = []
    for prediction in predictions:
        top_indices = prediction.argsort()[-top:][::-1]
        result = [(i, index_to_object_class(i), prediction[i],) for i in top_indices]
        result.sort(key=lambda x: x[2], reverse=True)
        results.append(result)
    return results


# Preprocess input procedure (same as MobileNet's implementation)
def preprocess_input(x):
    return imagenet_utils.preprocess_input(x, mode='tf')


# The target size the model needs
target_size = (128, 128)


# NOTE: layer, creations, as well as layers are created using MobileNet's implementation,
# but adapted to create a smaller model
# Procedure to create an instance of VisualAidNet
def VisualAidNet(input_shape=None,
                 alpha=1.0,
                 depth_multiplier=1,
                 dropout=1e-3,
                 include_top=True,
                 weights=None,
                 input_tensor=None,
                 classes=12):

    if not (weights is None or os.path.exists(weights)):
        current_dir = os.getcwd()
        raise ValueError('The `weights` argument should be either `None` (random initialization), or the path to the '
                         'weights file to be loaded.\ngiven:' + str(weights) + '\ncurrent directory:' + str(current_dir))

    # Determine proper input shape and default size.
    if input_shape is None:
        default_size = 224
    else:
        if K.image_data_format() == 'channels_first':
            rows = input_shape[1]
            cols = input_shape[2]
        else:
            rows = input_shape[0]
            cols = input_shape[1]

        if rows == cols and rows in [128, 160, 192, 224]:
            default_size = rows
        else:
            default_size = 224

    input_shape = _obtain_input_shape(input_shape,
                                      default_size=default_size,
                                      min_size=32,
                                      data_format=K.image_data_format(),
                                      require_flatten=include_top,
                                      weights=weights)

    if K.image_data_format() == 'channels_last':
        row_axis, col_axis = (0, 1)
    else:
        row_axis, col_axis = (1, 2)
    rows = input_shape[row_axis]

    if K.image_data_format() != 'channels_last':
        warnings.warn('VisualAidNet, based on MobileNet, is only available for the input data format "channels_last" '
                      '(width, height, channels). However your settings specify the default data format '
                      '"channels_first" (channels, width, height). You should set `image_data_format="channels_last"` '
                      'in your Keras config located at ~/.keras/keras.json. The model being returned right now will '
                      'expect inputs to follow the "channels_last" data format.')
        K.set_image_data_format('channels_last')
        old_data_format = 'channels_first'
    else:
        old_data_format = None

    if input_tensor is None:
        img_input = Input(shape=input_shape)
    else:
        if not K.is_keras_tensor(input_tensor):
            img_input = Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor

    x = _conv_block(img_input, 32, alpha, strides=(2, 2))
    x = _depthwise_conv_block(x, 64, alpha, depth_multiplier, block_id=1)

    x = _depthwise_conv_block(x, 128, alpha, depth_multiplier,
                              strides=(2, 2), block_id=2)
    x = _depthwise_conv_block(x, 128, alpha, depth_multiplier, block_id=3)

    if K.image_data_format() == 'channels_first':
        shape = (int(128 * alpha), 1, 1)  # 1024 * alpha
    else:
        shape = (1, 1, int(128 * alpha))  # 1024 * alpha

    x = GlobalAveragePooling2D()(x)
    x = Reshape(shape, name='reshape_1')(x)
    x = Dropout(dropout, name='dropout')(x)
    x = Conv2D(classes, (1, 1),
               padding='same', name='conv_preds')(x)
    x = Activation('softmax', name='act_softmax')(x)
    x = Reshape((classes,), name='reshape_2')(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
        inputs = get_source_inputs(input_tensor)
    else:
        inputs = img_input

    # Create model.
    model = Model(inputs, x, name='VisualAidNet%0.2f_%s' % (alpha, rows))

    # load weights
    if weights is not None:
        model.load_weights(weights)

    if old_data_format:
        K.set_image_data_format(old_data_format)
    return model
