# The file containing code for getting a summary for predictions of a given object class
import numpy as np


# Procedure to create a results summary
def create_results_summary(object_class, results):
    # Create the summary for a top-5
    top = 5

    # Get the prediction keys
    prediction_keys = results.keys()
    length_prediction_keys = len(prediction_keys)
    # The arrays of predictions and real values
    # noinspection PyTypeChecker
    predictions = np.full(length_prediction_keys * top, "", dtype=object)

    # Get the "final answer" for all images as input for the graph
    for idx, image_key in enumerate(prediction_keys):
        # Get the result
        result = results[image_key]
        # For the image results in top-x, check if the prediction is correct
        for n in range(1, top + 1):
            # Insert the result at the correct spot in the list
            predictions[idx + (n - 1) * length_prediction_keys] = result[n][0]

    # The dictionary containing the summary for each top (1, 3, 5), forcing object_class as first key
    prediction_summary = {1: {object_class: 0}, 3: {object_class: 0}, 5: {object_class: 0}}

    # Add all prediction to the predicted_values
    for idx, a_prediction in enumerate(predictions):
        # Remove '_' from class name (for "pretty" printing the results)
        prediction_name = a_prediction.replace("_", " ")
        # Top-1 only
        if idx < length_prediction_keys:
            # If the value isn't in summary_dictionary => add it, along with its count
            if prediction_name not in prediction_summary[1].keys():
                prediction_summary[1][prediction_name] = 1
            # Update the number of occurrences for that predicted value
            else:
                prediction_summary[1][prediction_name] += 1
        # Top-3 only
        if idx < 3 * length_prediction_keys:
            # If the value isn't in summary_dictionary => add it, along with its count
            if prediction_name not in prediction_summary[3].keys():
                prediction_summary[3][prediction_name] = 1
            # Update the number of occurrences for that predicted value
            else:
                prediction_summary[3][prediction_name] += 1
        # Top-5 only
        if idx < 5 * length_prediction_keys:
            # If the value isn't in summary_dictionary => add it, along with its count
            if prediction_name not in prediction_summary[5].keys():
                prediction_summary[5][prediction_name] = 1
            # Update the number of occurrences for that predicted value
            else:
                prediction_summary[5][prediction_name] += 1

    # Display the results summary
    print("-----------------------------------------------------------------------------------------")
    print("Results Summary for", len(results.keys()), "predictions")
    print("Object class:", object_class)
    for t in prediction_summary.keys():
        print("Top-" + str(t))
        summary = prediction_summary[t]
        for key in summary.keys():
            print(key, ":", summary[key], end="  |  ")
        print()
    print()
    # Return the summary
    return prediction_summary
