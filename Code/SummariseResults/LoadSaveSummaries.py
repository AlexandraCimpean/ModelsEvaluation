# The file containing procedures to load in and save results
from Resources import root_results_directory
import ast
import csv
import os


# Procedure for saving prediction results to a .csv file
def save_summary(model_name, object_class, summary):
    # Get the path to the destination file (assumes directory structure is present)
    file_path = os.path.join(root_results_directory, model_name, object_class, object_class + " summary.csv")
    # The fields of the dictionary
    fields = [1, 3, 5]
    # Save the results into the file
    with open(file_path, "w", newline='') as file:
        writer = csv.DictWriter(file, fields)
        writer.writeheader()
        writer.writerow(summary)


# Procedure to load prediction results from a .csv file
def load_summary(model_name, object_class):
    # Get the path to the source file (assumes directory structure is present)
    file_path = os.path.join(root_results_directory, model_name, object_class, object_class + " summary.csv")
    # The fields of the dictionary
    fields = [1, 3, 5]
    # Load the results from the file
    with open(file_path, 'r', newline='') as csv_file:
        reader = csv.DictReader(csv_file, fields)
        summary = {}
        for row in reader:
            # Ignore the header
            if row[1] == str(1):
                continue
            summary[1] = ast.literal_eval(row[1])
            summary[3] = ast.literal_eval(row[3])
            summary[5] = ast.literal_eval(row[5])

        # Return the summary
        return summary
