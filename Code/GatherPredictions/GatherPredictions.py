# The file containing procedures to gather the prediction results for one or more images, given an object class
from keras.preprocessing import image
import numpy as np
import os
from Resources import get_test_image_directory


# Procedure to preprocess an image into a numpy array
def image_to_numpy_array(image_path, preprocess_procedure, target_size):
    img = image.load_img(image_path, target_size=target_size)
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_procedure(x)
    return x


# The algorithm to get the top-1, top-3 and top-5 predictions for an image
# Returns a dictionary of the form:
#   {top-1 : [final answer, top-1 answer, top-1 probability, accepted?],
#    ...
#    top-5 : [final answer, top-5 answers, top-5 probability, accepted?]}
def get_prediction(dictionary, object_class, image_path,
                   model, preprocess_procedure, decode_procedure, target_size=(224, 224),
                   is_one_class_dictionary=False, is_VisualAidNet=False):
    # The results to return
    results_top_5 = {}

    # Preprocess the image for the model
    image_array = image_to_numpy_array(image_path, preprocess_procedure, target_size)
    predictions = model.predict(image_array)
    # Decode the results into a list of tuples (class node, description, probability)
    decoded_predictions = decode_procedure(predictions, top=5)[0]  # list of tuples (class, description, probability)

    # Get the accepted answers for the current object class
    if is_one_class_dictionary:
        accepted_answers = None
    else:
        accepted_answers = dictionary[object_class]

    # Compare the top-5 predictions
    for i in range(0, 5):
        # Get the top-i prediction
        top_i_prediction = decoded_predictions[i]

        # The model is the custom VisualAidNet model, trained on the object_classes ONLY
        if is_VisualAidNet:
            accepted = top_i_prediction[1] == object_class
            # If the answer is accepted, add the object_class as a final answer
            # else, add its own prediction as the final answer prediction
            if accepted:
                final_answer = object_class
            else:
                final_answer = top_i_prediction[1]
            # Get the result of the evaluation
            result_top_i = [final_answer, top_i_prediction[1], top_i_prediction[2], accepted]
            # Insert the result in the dictionary
            results_top_5[i + 1] = result_top_i

        # The model is a keras model, using ImageNet classes
        else:
            # The dictionary is 1 object class
            if is_one_class_dictionary:
                try:
                    accepted = dictionary[top_i_prediction[0]]
                except KeyError:
                    accepted = False
            # The dictionary contains one or more object classes
            else:
                accepted = top_i_prediction[0] in accepted_answers

            # If the answer is accepted, add the object_class as a final answer
            # else, add its own prediction as the final answer prediction
            if accepted:
                final_answer = object_class
            else:
                final_answer = top_i_prediction[1]
            # Get the result of the evaluation
            result_top_i = [final_answer, top_i_prediction[1], top_i_prediction[2], accepted]
            # Insert the result in the dictionary
            results_top_5[i + 1] = result_top_i

    # Return the results
    return results_top_5


# The algorithm for predicting multiple images per object class
def gather_multiple_predictions(dictionary, object_class,
                                model, preprocess_procedure, decode_procedure, target_size,
                                is_one_class_dictionary=False, is_VisualAidNet=False):
    # Get the path to the images
    images_directory_path = get_test_image_directory(object_class)
    # Extract the file names of the images (all images should be .jpg files (as prepared)
    images_file_names = [f for f in os.listdir(images_directory_path) if f.endswith('.jpg')]
    print(len(images_file_names), "images found in directory.\nGathering predictions...")

    # Create a dictionary, which maps image names onto their prediction results
    images_results = {}

    # Iterate over the images, and store their name and results in the dictionary
    for image_name in images_file_names:
        # Get the path to the image
        image_path = os.path.join(images_directory_path, image_name)
        # Get the results for the image
        image_results = get_prediction(dictionary, object_class, image_path, model, preprocess_procedure,
                                       decode_procedure, target_size, is_one_class_dictionary, is_VisualAidNet)
        # Insert the results into the dictionary, under the image's name
        images_results[image_name] = image_results

    print(len(images_results.keys()), "predictions gathered!")
    # Return the dictionary of results
    return images_results
