from Resources import root_results_directory
import csv
import os
import ast


# Procedure for saving prediction results to a .csv file
def save_results(model_name, object_class, results):
    # Get the path to the destination file
    file_path = os.path.join(root_results_directory, model_name, object_class, object_class + " results.csv")
    # The fields of the dictionary
    fields = ['image', 1, 2, 3, 4, 5]
    # Save the results into the file
    with open(file_path, "w", newline='') as file:
        writer = csv.DictWriter(file, fields)
        writer.writeheader()
        for key in results.keys():
            writer.writerow({field: results[key].get(field) or key for field in fields})


# Procedure to load prediction results from a .csv file
def load_results(model_name, object_class):
    # Get the path to the source file
    file_path = os.path.join(root_results_directory, model_name, object_class, object_class + " results.csv")
    # The fields of the dictionary
    fields = ['image', 1, 2, 3, 4, 5]
    # Load the results from the file
    with open(file_path, 'r', newline='') as csv_file:
        reader = csv.DictReader(csv_file, fields)
        results = {}
        for row in reader:
            # Get the image name as the key
            key = row["image"]
            # Skip the header
            if key == "image":
                continue
            # Create the dictionary for the image's predictions
            image_results = {1: ast.literal_eval(row[1]), 2: ast.literal_eval(row[2]), 3: ast.literal_eval(row[3]),
                             4: ast.literal_eval(row[4]), 5: ast.literal_eval(row[5])}
            # Insert the image results into the results dictionary
            results[key] = image_results

        # Return the results
        return results
