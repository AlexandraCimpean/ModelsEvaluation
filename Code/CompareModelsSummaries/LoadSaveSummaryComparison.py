from Resources import root_results_directory
import csv
import os


# Procedure for saving a summary comparison to a .csv file
def save_summary_comparison(object_class, summary_comparisons):
    # Get the path to the destination file
    file_path = os.path.join(root_results_directory, "Summary Comparisons", object_class,
                             object_class + " summary comparison top-")
    # Save top-1, 3 and 5 separately
    file_path_1 = file_path + "1.csv"
    file_path_3 = file_path + "3.csv"
    file_path_5 = file_path + "5.csv"

    # The fields of the dictionaries
    # fields = ["prediction", "MobileNet", "NASNetMobile", "ResNet50"]
    fields = ["prediction", "VisualAidNet_0.25", "VisualAidNet_0.50", "VisualAidNet_0.75", "VisualAidNet_1.00"]

    # Save the summary comparisons
    # Top-1
    with open(file_path_1, "w", newline='') as file:
        writer = csv.DictWriter(file, fields)
        writer.writeheader()
        for prediction in summary_comparisons[1].keys():
            row_dictionary = {field: summary_comparisons[1][prediction].get(field) or prediction for field in fields}
            for key in row_dictionary.keys():
                if key != "prediction" and row_dictionary[key] == prediction:
                    row_dictionary[key] = 0
            writer.writerow(row_dictionary)
    # Top-3
    with open(file_path_3, "w", newline='') as file:
        writer = csv.DictWriter(file, fields)
        writer.writeheader()
        for prediction in summary_comparisons[3].keys():
            row_dictionary = {field: summary_comparisons[3][prediction].get(field) or prediction for field in fields}
            for key in row_dictionary.keys():
                if key != "prediction" and row_dictionary[key] == prediction:
                    row_dictionary[key] = 0
            writer.writerow(row_dictionary)
    # Top-5
    with open(file_path_5, "w", newline='') as file:
        writer = csv.DictWriter(file, fields)
        writer.writeheader()
        for prediction in summary_comparisons[5].keys():
            row_dictionary = {field: summary_comparisons[5][prediction].get(field) or prediction for field in fields}
            for key in row_dictionary.keys():
                if key != "prediction" and row_dictionary[key] == prediction:
                    row_dictionary[key] = 0
            writer.writerow(row_dictionary)


# Procedure to load a saving a summary comparison from a .csv file
def load_summary_comparison(object_class):
    # Get the path to the source file
    file_path = os.path.join(root_results_directory, "Summary Comparisons", object_class,
                             object_class + " summary comparison top-")
    # Load top-1, 3 and 5 separately
    file_path_1 = file_path + "1.csv"
    file_path_3 = file_path + "3.csv"
    file_path_5 = file_path + "5.csv"

    # The fields of the dictionaries
    fields = ["prediction", "MobileNet", "NASNetMobile", "ResNet50"]

    # The dictionary, containing the top-1, top-3 and top-5 summary comparisons
    summary_comparisons = {}

    # Load the summary comparisons
    # Top-1
    with open(file_path_1, "r", newline='') as file:
        reader = csv.DictReader(file, fields)
        summary = {}
        for row in reader:
            # Get the prediction as the key
            key = row["prediction"]
            # Skip the header
            if key == "prediction":
                continue
            # Create the dictionary for the prediction
            prediction_summary = {fields[1]: int(row[fields[1]]), fields[2]: int(row[fields[2]]),
                                  fields[3]: int(row[fields[3]])}
            # Insert the prediction summary into the summary dictionary
            summary[key] = prediction_summary

        # Insert the summary into the summary_comparisons dictionary
        summary_comparisons[1] = summary
    # Top-3
    with open(file_path_3, "r", newline='') as file:
        reader = csv.DictReader(file, fields)
        summary = {}
        for row in reader:
            # Get the prediction as the key
            key = row["prediction"]
            # Skip the header
            if key == "prediction":
                continue
            # Create the dictionary for the prediction
            prediction_summary = {fields[1]: int(row[fields[1]]), fields[2]: int(row[fields[2]]),
                                  fields[3]: int(row[fields[3]])}
            # Insert the prediction summary into the summary dictionary
            summary[key] = prediction_summary

        # Insert the summary into the summary_comparisons dictionary
        summary_comparisons[3] = summary
    # Top-5
    with open(file_path_5, "r", newline='') as file:
        reader = csv.DictReader(file, fields)
        summary = {}
        for row in reader:
            # Get the prediction as the key
            key = row["prediction"]
            # Skip the header
            if key == "prediction":
                continue
            # Create the dictionary for the prediction
            prediction_summary = {fields[1]: int(row[fields[1]]), fields[2]: int(row[fields[2]]),
                                  fields[3]: int(row[fields[3]])}
            # Insert the prediction summary into the summary dictionary
            summary[key] = prediction_summary

        # Insert the summary into the summary_comparisons dictionary
        summary_comparisons[5] = summary

    # Return the summary_comparisons dictionary
    return summary_comparisons
