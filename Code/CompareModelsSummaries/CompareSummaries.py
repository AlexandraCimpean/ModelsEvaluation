from SummariseResults.LoadSaveSummaries import load_summary


# Procedure to compare the results summaries of models for a given object class
def compare_summaries(model_names, object_class):
    # The dictionary, containing the summaries for an object class
    comparison_models_summary = {1: {}, 3: {}, 5: {}}

    # For each top
    for top in [1, 3, 5]:
        # For each model
        for model_name in model_names:
            # Get the summary
            model_summary = load_summary(model_name, object_class)
            # For each key (prediction) in the dictionary, add the models count to the comparisons summary
            for prediction in model_summary[top].keys():
                # Check if there already is an entry for prediction
                try:
                    comparison_models_summary[top][prediction][model_name] = model_summary[top][prediction]
                # There is no entry yet for the prediction
                except KeyError:
                    comparison_models_summary[top][prediction] = {}
                    # Initialise the count for all the models
                    for name in model_names:
                        if name == model_name:
                            comparison_models_summary[top][prediction][model_name] = model_summary[top][prediction]
                        else:
                            comparison_models_summary[top][prediction][name] = 0

    # Return the comparison summary
    return comparison_models_summary


if __name__ == '__main__':

    from Models.MobileNet import name_MobileNet
    from Models.NASNetMobile import name_NASNetMobile
    from Models.ResNet50 import name_ResNet50
    from Models.VisualAidNet import name_VisualAidNet_025, name_VisualAidNet_050, name_VisualAidNet_075,\
        name_VisualAidNet_100
    from CompareModelsSummaries.LoadSaveSummaryComparison import save_summary_comparison
    from ObjectDictionaries import *

    # Get the model names
    # names = [name_MobileNet, name_NASNetMobile, name_ResNet50]
    names = [name_VisualAidNet_025, name_VisualAidNet_050, name_VisualAidNet_075, name_VisualAidNet_100]

    # For each object class, get the summary comparisons
    for object_class in object_classes_dictionary.keys():
        # Get the comparison summary
        comparison_summary = compare_summaries(names, object_class)
        # Save it to a file
        save_summary_comparison(object_class, comparison_summary)
    # Dog class
    # Get the comparison summary
    comparison_summary = compare_summaries(names, dog_class)
    # Save it to a file
    save_summary_comparison(dog_class, comparison_summary)
