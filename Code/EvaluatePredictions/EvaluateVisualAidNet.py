import time
import warnings
import os

from Resources import root_results_directory
from EvaluatePredictions.EvaluatePretrainedModels import evaluate_object_classes
from Models.VisualAidNet import preprocess_VisualAidNet, decode_VisualAidNet, target_size_VisualAidNet, \
    create_VisualAidNet, name_VisualAidNet_025, name_VisualAidNet_050, name_VisualAidNet_075, name_VisualAidNet_100


if __name__ == '__main__':
    warnings.filterwarnings('ignore')

    # Set the seed
    # PYTHONHASHSEED=0 added to interpreter
    seed = 1000
    import random, tensorflow, numpy
    random.seed(seed)
    tensorflow.set_random_seed(seed)
    numpy.random.seed(seed)

    # Get the time at start (for the entire process)
    complete_starting_time = time.time()

    # The list of model names
    model_names = [name_VisualAidNet_025, name_VisualAidNet_050, name_VisualAidNet_075, name_VisualAidNet_100]
    # The list of models
    models = [create_VisualAidNet(alpha=0.25), create_VisualAidNet(alpha=0.50), create_VisualAidNet(alpha=0.75),
              create_VisualAidNet(alpha=1.00)]

    # Procedure to scale an array, representing image data
    def scale_img(x):
        x *= 1. / 255
        return x

    # The preprocess procedure to use, depending on how the images were preprocessed during the training of the model
    preprocess_procedure = scale_img  # preprocess_VisualAidNet

    # For each model, evaluate ALL object classes images and save the results
    for idx in range(len(model_names)):

        # Assign the current model
        current_model = models[idx]
        current_model_name = model_names[idx]
        print("Evaluating", current_model_name)

        # Open a file to store in the times
        with open(os.path.join(root_results_directory, "Times", current_model_name + ".txt"), "w", newline='') as file:

            # Evaluate all object classes with the given model
            total_classes_time, all_times = evaluate_object_classes(current_model_name, current_model,
                                                                    preprocess_procedure, decode_VisualAidNet,
                                                                    target_size_VisualAidNet, is_VisualAidNet=True)

            # Write the time taken for all classes, and for each class separately
            file.write("All classes time: " + str(total_classes_time) + " seconds\n")
            file.flush()
            for key in all_times.keys():
                file.write(key + " time: " + str(all_times[key]) + " seconds\n")
            file.flush()
            # Close the file
            file.close()

        print("\n\n\n")

    # Get the time at finish (for the entire process)
    complete_finish_time = time.time()
    print("-=-=-=- Time for entire process:     ", complete_finish_time - complete_starting_time, "seconds -=-=-=-")

    warnings.filterwarnings('always')
