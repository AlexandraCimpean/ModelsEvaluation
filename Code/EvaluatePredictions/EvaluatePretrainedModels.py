from GatherPredictions.GatherPredictions import gather_multiple_predictions
from GatherPredictions.LoadSaveResults import save_results
from SummariseResults.ResultsSummary import create_results_summary
from SummariseResults.LoadSaveSummaries import save_summary
from ObjectDictionaries import *
import time
import warnings


# Procedure to evaluate an object class
def evaluate_object_class(object_class, object_dictionary, is_one_class_dictionary,
                          model_name, model, preprocess_procedure, decode_procedure, target_size,
                          is_VisualAidNet=False):
    print("=========================================================================================")
    print(object_class)
    print("-----------------------------------------------------------------------------------------")

    # Get the time at the start of the object class evaluation
    start_time = time.time()

    # Gather predictions
    results = gather_multiple_predictions(object_dictionary, object_class,
                                          model, preprocess_procedure, decode_procedure, target_size,
                                          is_one_class_dictionary, is_VisualAidNet)
    # Save the results
    save_results(model_name, object_class, results)

    # Summarise results
    summary = create_results_summary(object_class, results)
    # Save the summaries
    save_summary(model_name, object_class, summary)

    time_passed = time.time() - start_time
    print("-----------------------------------------------------------------------------------------")
    print("Time passed:", time_passed, "seconds")
    print("=========================================================================================")

    # Return the amount of time passed
    return time_passed


# Procedure to execute B, C, and D and save the results/summaries/statistics gathered
def evaluate_object_classes(model_name, model, preprocess_procedure, decode_procedure, target_size,
                            is_VisualAidNet=False):
    # The dictionary of object_classes and their time for evaluating
    object_classes_times = {}

    # Get the time at the start of the object classes evaluation
    start_time = time.time()

    # For all object classes
    for object_class in object_classes_dictionary.keys():
        object_classes_times[object_class] = evaluate_object_class(object_class, object_classes_dictionary, False,
                                                                   model_name, model, preprocess_procedure,
                                                                   decode_procedure, target_size, is_VisualAidNet)
    # For dog class
    object_classes_times[dog_class] = evaluate_object_class(dog_class, dog_class_dictionary, True,
                                                            model_name, model, preprocess_procedure,
                                                            decode_procedure, target_size, is_VisualAidNet)

    time_passed = time.time() - start_time
    print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
    print("Total time passed:", time_passed, "seconds")
    print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")

    # Return the total time passed, along with the time per object class
    return time_passed, object_classes_times


if __name__ == '__main__':
    warnings.filterwarnings('ignore')

    # Set the seed
    # PYTHONHASHSEED=0 added to interpreter
    seed = 1000
    import random, tensorflow, numpy
    random.seed(seed)
    tensorflow.set_random_seed(seed)
    numpy.random.seed(seed)

    # Get the time at start (for the entire process)
    complete_starting_time = time.time()

    # Script to evaluate all models
    # In alphabetical order => 1st MobileNet, 2nd NASNetMobile, 3rd RestNet50
    from Models.MobileNet import *
    from Models.NASNetMobile import *
    from Models.ResNet50 import *

    # The list of model names
    model_names = [name_MobileNet, name_NASNetMobile, name_ResNet50]
    # The list of model create procedures
    model_create_procedures = [create_MobileNet, create_NASNetMobile, create_ResNet50]
    # The list of preprocess procedures
    model_preprocess_procedures = [preprocess_MobileNet, preprocess_NASNetMobile, preprocess_ResNet50]
    # The list of decoding procedures
    model_decode_procedures = [decode_MobileNet, decode_NASNetMobile, decode_ResNet50]
    # The list of target sizes
    model_target_sizes = [target_size_MobileNet, target_size_NASNetMobile, target_size_ResNet50]

    # For each model, evaluate ALL object classes images and save the results
    for idx in range(len(model_names)):

        # Assign the current model
        current_model = model_create_procedures[idx]()

        # Open a file to store in the times TODO: change directory
        with open("/Users/alexandracimpean/Desktop/" + model_names[idx] + ".txt", "w", newline='') as file:

            print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-")
            print(model_names[idx])

            # Evaluate all object classes with the given model
            total_classes_time, all_times = evaluate_object_classes(model_names[idx], current_model,
                                                                    model_preprocess_procedures[idx],
                                                                    model_decode_procedures[idx],
                                                                    model_target_sizes[idx])

            # Write the time taken for all classes, and for each class separately
            file.write("All classes time: " + str(total_classes_time) + " seconds\n")
            file.flush()
            for key in all_times.keys():
                file.write(key + " time: " + str(all_times[key]) + " seconds\n")
            file.flush()
            # Close the file
            file.close()

        print("\n\n\n")

    # Get the time at finish (for the entire process)
    complete_finish_time = time.time()
    print("-=-=-=- Time for entire process:     ", complete_finish_time - complete_starting_time, "seconds -=-=-=-")

    warnings.filterwarnings('always')
