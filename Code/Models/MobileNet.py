# MobileNet
from keras.applications.mobilenet import MobileNet, preprocess_input, decode_predictions

# The name of the MobileNet model (will be used for saving, loading in results)
name_MobileNet = "MobileNet"

# The image preprocess procedure
preprocess_MobileNet = preprocess_input
# The predictions decoding procedure
decode_MobileNet = decode_predictions
# The image target size
target_size_MobileNet = (224, 224)


# Procedure to create an instance of MobileNet, with ImageNet weights
def create_MobileNet():
    return MobileNet(weights='imagenet')
