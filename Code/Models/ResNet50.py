# ResNet50
from keras.applications.resnet50 import ResNet50, preprocess_input, decode_predictions

# The name of the ResNet50 model (will be used for saving, loading in results)
name_ResNet50 = "ResNet50"

# The image preprocess procedure
preprocess_ResNet50 = preprocess_input
# The predictions decoding procedure
decode_ResNet50 = decode_predictions
# The image target size
target_size_ResNet50 = (224, 224)


# Procedure to create an instance of ResNet50, with ImageNet weights
def create_ResNet50():
    return ResNet50(weights='imagenet')
