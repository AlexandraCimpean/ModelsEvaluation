# NASNetMobile
from keras.applications.nasnet import NASNetMobile, preprocess_input, decode_predictions

# The name of the NASNetMobile model (will be used for saving, loading in results)
name_NASNetMobile = "NASNetMobile"

# The image preprocess procedure
preprocess_NASNetMobile = preprocess_input
# The predictions decoding procedure
decode_NASNetMobile = decode_predictions
# The image target size
target_size_NASNetMobile = (224, 224)


# Procedure to create an instance of NASNetMobile, with ImageNet weights
def create_NASNetMobile():
    return NASNetMobile(weights='imagenet')
