# VisualAidNet
from VisualAidNetTraining.VisualAidNetModel import VisualAidNet, preprocess_input, decode_predictions

# The name of the VisualAidNet model (will be used for saving, loading in results)
name_VisualAidNet = "VisualAidNet"
name_VisualAidNet_100 = name_VisualAidNet + "_1.00"
name_VisualAidNet_075 = name_VisualAidNet + "_0.75"
name_VisualAidNet_050 = name_VisualAidNet + "_0.50"
name_VisualAidNet_025 = name_VisualAidNet + "_0.25"

# The image preprocess procedure
preprocess_VisualAidNet = preprocess_input
# The predictions decoding procedure
decode_VisualAidNet = decode_predictions
# The image target size
target_size_VisualAidNet = (128, 128)


# Procedure to create an instance of VisualAidNet, with our own weights
# NOTE; weights are from models trained on 50% of data, using RMSprop and MSE
def create_VisualAidNet(alpha=1.00, weights=None):
    # The input shape
    input_shape = (128, 128, 3)
    # The path to the weights
    if weights is None:
        weights = "../Models/VisualAidNet_Weights/VisualAidNet_"

        if alpha == 1.00:
            weights += "1.00.hdf5"
        elif alpha == 0.75:
            weights += "0.75.hdf5"
        elif alpha == 0.50:
            weights += "0.50.hdf5"
        elif alpha == 0.25:
            weights += "0.25.hdf5"
        else:
            raise ValueError(alpha)

    # Create an instance of the model for the given alpha, and load in the weights
    return VisualAidNet(alpha=alpha, input_shape=input_shape, include_top=False, weights=weights)
