# Script to prepare all images in a given folder
import os


# Procedure to check if a path points to an image file
def is_image(path):
    return os.path.isfile(path) and \
        (path.endswith("jpg") or path.endswith("jpeg") or path.endswith("png") or path.endswith("gif") or
         path.endswith("bmp") or path.endswith("ash") or path.endswith("fpx") or path.endswith("jpe"))


# Procedure to move all files from a sub-directory into the provided root-directory
def move_all_to_directory(root_directory_path, current_directory_path=None):
    # The current directory
    if current_directory_path is None:
        current_directory_path = root_directory_path
    # Get a list of all the folders and files in the root directory
    all_file_names = os.listdir(current_directory_path)
    # The number of images moved
    current_images_moved = 0

    if current_directory_path == root_directory_path:
        print("Moving all images from sub-directories into", root_directory_path, "...")
        print(len(all_file_names), "directories and/or files detected in root directory. (hidden included)")

    # Loop over the files/directories
    for file_name in all_file_names:
        # Get the path to the file/directory
        file_path = current_directory_path + "/" + file_name
        # The file is a directory
        if os.path.isdir(file_path):
            # Loop over the files in the sub-directory
            current_images_moved += move_all_to_directory(root_directory_path, file_path)
        # The file is an image
        elif is_image(file_path):
            # Get the move the image to the root directory
            new_image_path = root_directory_path + "/" + file_name
            # Rename the image to move it
            os.rename(file_path, new_image_path)
            # Update counter
            current_images_moved += 1

    if current_directory_path == root_directory_path:
        print(current_images_moved, "Images successfully moved!")
    else:
        return current_images_moved


# Procedure to rename all images in a directory, starting from start_number
def rename_images(directory_path, start_number=1):
    # Get a list of all the images names in the folder (Note: will be shuffled, which is preferred)
    images_names = os.listdir(directory_path)
    # The current number to be used as name
    number = start_number

    print("Renaming all the images...")
    images_renamed = 0

    for image_name in images_names:
        # Get the path to the image
        image_path = directory_path + "/" + image_name
        # Make sure were holding an image file
        if is_image(image_path):
            # Get the new path for the image
            new_image_path = directory_path + "/" + str(number) + ".jpg"
            # Rename the image
            os.rename(image_path, new_image_path)
            # Update number
            number += 1
            images_renamed += 1

    print(images_renamed, "Images successfully renamed!")


# Procedure to move all images from a directory's sub-directories, and then rename them, starting from a given number
def prepare_images(directory_path, start_number=1):
    # First, move all images from sub-directories into the given directory
    move_all_to_directory(directory_path)
    # Then, rename all images numerically, starting from start_number
    rename_images(directory_path, start_number)
