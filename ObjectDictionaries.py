# The file containing the dictionaries for the object classes
import numpy

# The classes
apple_fruit_class = "apple fruit"
bell_pepper_class = "bell pepper"
orange_fruit_class = "orange fruit"
bread_class = "bread"
mushroom_class = "mushroom"
bottle_class = "bottle"
wallet_class = "wallet"
candle_class = "candle"
lamp_class = "lamp"
chair_class = "chair"
dog_class = "dog"
cat_class = "cat"


# The mapping of an object name to the node id's used for the ImageNet class.
object_classes_dictionary = {
    apple_fruit_class: ["n07742313"],
    bell_pepper_class: ["n07720875"],
    bottle_class: ["n02823428", "n03983396", "n04557648", "n04591713"],
    bread_class: ["n07684084", "n07693725"],
    candle_class: ["n02948072"],
    cat_class: ["n02123045", "n02123159", "n02123394", "n02123597", "n02124075"],
    chair_class: ["n04099969", "n03376595", "n02791124"],
    lamp_class: ["n04380533", "n03637318"],
    mushroom_class: ["n07734744"],
    orange_fruit_class: ["n07747607"],
    wallet_class: ["n04548362"],
}


# NOTE: because the number of class nodes for "dog", the class "dog" will be a dictionary itself, in order to
#       avoid bad performance due to linear search in a list
dog_class_dictionary = {
    "n02085620": True, "n02085782": True, "n02085936": True, "n02086079": True, "n02086240": True,
    "n02086646": True, "n02086910": True, "n02087046": True, "n02087394": True, "n02088094": True,
    "n02088238": True, "n02088364": True, "n02088466": True, "n02088632": True, "n02089078": True,
    "n02089867": True, "n02089973": True, "n02090379": True, "n02090622": True, "n02090721": True,
    "n02091032": True, "n02091134": True, "n02091244": True, "n02091467": True, "n02091635": True,
    "n02091831": True, "n02092002": True, "n02092339": True, "n02093256": True, "n02093428": True,
    "n02093647": True, "n02093754": True, "n02093859": True, "n02093991": True, "n02094114": True,
    "n02094258": True, "n02094433": True, "n02095314": True, "n02095570": True, "n02095889": True,
    "n02096051": True, "n02096177": True, "n02096294": True, "n02096437": True, "n02096585": True,
    "n02097047": True, "n02097130": True, "n02097209": True, "n02097298": True, "n02097474": True,
    "n02097658": True, "n02098105": True, "n02098286": True, "n02098413": True, "n02099267": True,
    "n02099429": True, "n02099601": True, "n02099712": True, "n02099849": True, "n02100236": True,
    "n02100583": True, "n02100735": True, "n02100877": True, "n02101006": True, "n02101388": True,
    "n02101556": True, "n02102040": True, "n02102177": True, "n02102318": True, "n02102480": True,
    "n02102973": True, "n02104029": True, "n02104365": True, "n02105056": True, "n02105162": True,
    "n02105251": True, "n02105412": True, "n02105505": True, "n02105641": True, "n02105855": True,
    "n02106030": True, "n02106166": True, "n02106382": True, "n02106550": True, "n02106662": True,
    "n02107142": True, "n02107312": True, "n02107574": True, "n02107683": True, "n02107908": True,
    "n02108000": True, "n02108089": True, "n02108422": True, "n02108551": True, "n02108915": True,
    "n02109047": True, "n02109525": True, "n02109961": True, "n02110063": True, "n02110185": True,
    "n02110341": True, "n02110627": True, "n02110806": True, "n02110958": True, "n02111129": True,
    "n02111277": True, "n02111500": True, "n02111889": True, "n02112018": True, "n02112137": True,
    "n02112350": True, "n02112706": True, "n02113023": True, "n02113186": True, "n02113624": True,
    "n02113712": True, "n02113799": True, "n02113978": True
}


# The list of all object classes (sorted alphabetically)
all_object_classes = numpy.array([apple_fruit_class, bell_pepper_class, bottle_class, bread_class, candle_class,
                                  cat_class, chair_class, dog_class, lamp_class, mushroom_class, orange_fruit_class,
                                  wallet_class])
